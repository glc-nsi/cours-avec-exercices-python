# Journée de formation du 2 juillet 2024 - NEW 22/10/2024

### Ceci est la **page d'accueil** de notre site.

Vous pouvez y ajouter du contenu, des images, des liens, etc.

## Liens de bases

   * [Lien vers la page 1](page1)
   * [Lien vers la forge](https://nsinormandie.forge.apps.education.fr/2024-intro-forge/)
   * etc 

## Ex 1 

voici le premier **exo** : $\int_0^{\alpha} \frac{1}{1+t^2} dt$

## Ex 2

voici le deuxième **exo** : 

$$
\int_0^{\alpha} \frac{1}{1+t^2} dt
$$

## Ex 3

voici le troisième **exo** : 

$$
\sqrt{2}
$$


## Ex4

Voir la page [page2](page2)

# IDE

Compléter le script ci-dessous.
N'oubliez pas de valider les tests après avoir exécuté.    

{{ IDE() }}
